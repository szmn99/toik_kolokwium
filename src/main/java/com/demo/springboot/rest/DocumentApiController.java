package com.demo.springboot.rest;

import com.demo.springboot.dto.ArrayDoDto;
import com.demo.springboot.service.ShipsService;
import com.demo.springboot.service.ShipsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DocumentApiController {

    DocumentApiController() {

    }

    private static final Logger LOG = LoggerFactory.getLogger(DocumentApiController.class);

    ShipsService ss = new ShipsServiceImpl();


    @PostMapping
    @RequestMapping("/api/shot")
    public ResponseEntity<Void> solve(@RequestParam int position) {
        //ArrayDoDto x = ss.solve(position);
        //LOG.info("--- get all movies: {}", movies.getMovies());
        if(ss.solve(position) =='1'){
            LOG.info("trafiony");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else if(ss.solve(position) =='2'){
            LOG.info("pudło");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
