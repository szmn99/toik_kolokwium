package com.demo.springboot.dto;

public class ArrayDoDto {
    Integer x1;

    public ArrayDoDto(Integer x1) {
        this.x1 = x1;
    }

    public Integer getX1() {
        return x1;
    }
}
